# -*- coding: utf-8 -*-
'''
	控制台相关
'''

from . import fs

# 进度块
PROGRESSBLOCK = 32

def downloadProgress(nsize, totalsize, speed, note = ""):
	''' 
		下载进度 
	'''
	prec = int(nsize / totalsize * 100)
	blockbar = int(PROGRESSBLOCK * nsize / totalsize)
	emptybar = PROGRESSBLOCK - blockbar
	print("%s  %3d%% |%s%s| %s %s/s" % (note, prec,"█"*blockbar,"  "*emptybar, fs.formatFileSize(nsize,7), fs.formatFileSize(speed,7)), end = "\r")

def workProgress(count, total, notef = "", notel = ""):	
	''' 
		工作进度 
	'''
	prec = int(count / total * 100)
	blockbar = int(PROGRESSBLOCK * count / total)
	emptybar = PROGRESSBLOCK - blockbar
	print("%s  %3d%% |%s%s| %s/%s  %s" % (notef, prec,"█"*blockbar,"  "*emptybar, count, total, notel), end = "\r")
