# -*- coding: utf-8 -*-

import logging
from ..preprocess import REG_CALL
from .variables import GLOBALS

def call(table):
	'''
		执行call调用
	'''
	keys = None
	if isinstance(table, list):
		keys = range(0, len(table))
	elif isinstance(table, dict):
		keys = table.keys()
	if keys:
		for k in keys:
			v = table[k]
			if isinstance(v, (list, dict)):
				if not call(v):
					return False
			elif isinstance(v, str):
				m = REG_CALL.match(v)
				if m:
					code = m.group(1).upper() or 'P'
					source = m.group(2)
					if code == 'P':
						table[k] = eval(source, GLOBALS)
					else:
						logging.error('unsupport lang code `%s`' % code)
						return False
	return True
