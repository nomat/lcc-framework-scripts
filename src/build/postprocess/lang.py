# -*- coding: utf-8 -*-

import os,logging,shutil
from ...utils import py
from ...utils import fs
from .process import call as processCall
from ..caches.lang_cache import LangCache
from .variables import GLOBALS,DATAS

LangDefineIndex = 0

def __langDefine(*config):
	'''
		语言定义
	'''
	global LangDefineIndex
	pack = os.path.basename(GLOBALS['__workPath'])
	langCache = LangCache(os.path.join(GLOBALS['__workPath'], 'postprocess', 'langs', 'langs.json'))
	langCache.load()

	langConfigs = []
	for c in config:
		lang = c[0]
		text = c[1]
		fontName = c[2] if len(c) > 2 else None
		fontSize = c[3] if len(c) > 3 else None
		key = langCache.findLangText(lang, text, fontName, fontSize)
		langConfigs.append({
			'lang' : lang,
			'text' : text,
			'fontName' : fontName,
			'fontSize' : fontSize,
			'key' : key
		})

	# 检测配置冲突
	langKey = None
	conflict = False
	for lc in langConfigs:
		key = lc.get('key')
		if key:
			if langKey == None:
				langKey = key
			elif langKey != key:
				conflict = True
				break
	if conflict or langKey == None:
		langKey = '%s_%d' % (pack, LangDefineIndex)
		LangDefineIndex += 1
	
	# 添加语言文本
	for lc in langConfigs:
		if conflict or lc.get('key') == None:
			langCache.addLangText(lc.get('lang'), langKey, lc.get('text'), lc, lc.get('fontName'), lc.get('fontSize'))

	langCache.save()
	return langKey

def __langText(lang, key):
	'''
		语言文本引用
	'''
	return DATAS['langs'][lang]['texts'][key]['text']

def __langImage(lang, key):
	'''
		语言图片引用
	'''
	return DATAS['langs'][lang]['images'][key]

def setup(buildPath):
	'''
		安装语言功能
	'''

	# 数据加载
	indexs = {}
	DATAS['indexs'] = indexs
	langs = {}
	DATAS['langs'] = langs
	for pack in os.listdir(buildPath):
		fontPath = os.path.join(buildPath, pack, 'compile', 'langs', 'fonts-index.json')
		if os.path.exists(fontPath):
			table = py.readJson(fontPath)['cache']
			py.mergeDict(indexs, table)
		langPath = os.path.join(buildPath, pack, 'compile', 'langs', 'langs.json')
		if os.path.exists(langPath):
			table = py.readJson(langPath)['cache']
			py.mergeDict(langs, table)

	# 全局定义
	GLOBALS['langDefine'] = __langDefine
	GLOBALS['langText'] = __langText
	GLOBALS['langImage'] = __langImage

	return True

def process(buildPath):
	'''
		处理语言功能
	'''
	for pack in os.listdir(buildPath):
		fs.clearDirectory(os.path.join(buildPath, pack, 'postprocess', 'langs'))
		
		fontPath = os.path.join(buildPath, pack, 'compile', 'langs', 'fonts-index.json')
		fontOutPath = os.path.join(buildPath, pack, 'postprocess', 'langs', 'fonts-index.json')
		if os.path.exists(fontPath):
			table = py.readJson(fontPath)
			GLOBALS['__workPath'] = os.path.join(buildPath, pack)
			if not processCall(table):
				logging.error('font process call fail `%s`' % fontPath)
				return False
			fontOutDir = os.path.dirname(fontOutPath)
			if not os.path.exists(fontOutDir):
				os.makedirs(fontOutDir)
			py.writeJson(table, fontOutPath, True)
			
		langPath = os.path.join(buildPath, pack, 'compile', 'langs', 'langs.json')
		langOutPath = os.path.join(buildPath, pack, 'postprocess', 'langs', 'langs.json')
		if os.path.exists(langPath):
			table = py.readJson(langPath)
			GLOBALS['__workPath'] = os.path.join(buildPath, pack)
			if not processCall(table):
				logging.error('lang process call fail `%s`' % langPath)
				return False
			langOutDir = os.path.dirname(langOutPath)
			if not os.path.exists(langOutDir):
				os.makedirs(langOutDir)
			py.writeJson(table, langOutPath, True)
			
	return True
