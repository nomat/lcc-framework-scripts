# -*- coding: utf-8 -*-

import os,logging
from .lang import setup as setupLang
from .table import setup as setupTable
from .lang import process as processLang
from .table import process as processTable

def postprocessProject(buildPath):
	'''
		后处理工程
		@param buildPath 构建路径
	'''
	if not setupLang(buildPath):
		logging.error('lang setup fail')
		return False

	if not setupTable(buildPath):
		logging.error('table setup fail')
		return False
	
	if not processLang(buildPath):
		logging.error('lang process fail')
		return False

	if not processTable(buildPath):
		logging.error('table process fail')
		return False
	
	return True
