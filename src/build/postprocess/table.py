# -*- coding: utf-8 -*-

import os,logging,shutil
from ...utils import py
from ...utils import fs
from .process import call as processCall
from .variables import GLOBALS,DATAS

def __assetPath(localpath):
	pack = os.path.basename(GLOBALS['__workPath'])
	localpath = os.path.normpath(os.path.splitext(localpath)[0]).replace("\\","/")
	return 'asset://%s/lcc-assets/assets/%s' % (pack, localpath)

def __tableValue(tname, path = None):
	'''
		table值引用
	'''
	if path == None:
		return py.getDictPath(DATAS['tables'], tname)
	else:
		return py.getDictPath(DATAS['tables'][tname], path)

def setup(buildPath):
	'''
		安装数据表功能
	'''

	tables = {}
	DATAS['tables'] = tables
	for pack in os.listdir(buildPath):
		inPath = os.path.join(buildPath, pack, 'compile', 'tables')
		if os.path.exists(inPath):
			for tfile in os.listdir(inPath):
				table = py.readJson(os.path.join(inPath, tfile))['cache']
				py.mergeDict(tables, table)

	# 全局定义
	GLOBALS['assetPath'] = __assetPath
	GLOBALS['tableValue'] = __tableValue

	return True

def process(buildPath):
	'''
		处理数据表
	'''
	for pack in os.listdir(buildPath):
		fs.clearDirectory(os.path.join(buildPath, pack, 'postprocess', 'tables'))
		
		inPath = os.path.join(buildPath, pack, 'compile', 'tables')
		outPath = os.path.join(buildPath, pack, 'postprocess', 'tables')
		if os.path.exists(inPath):
			for tfile in os.listdir(inPath):
				tpath = os.path.join(inPath, tfile)
				table = py.readJson(tpath)
				GLOBALS['__workPath'] = os.path.join(buildPath, pack)
				if not processCall(table):
					logging.error('table process call fail `%s`' % tpath)
					return False
				if not os.path.exists(outPath):
					os.makedirs(outPath)
				outFile = os.path.join(outPath, tfile)
				py.writeJson(table, outFile, True)

	return True
