# -*- coding: utf-8 -*-

import os,logging
from ...utils import py
from ...utils import fs
from ..preprocess.name_field import NameField,TYPEFIELDS
from ..preprocess.value import toRealValue

class TableCollector:
	'''
		表收集器
	'''
	def __init__(self):
		self._tableMap = {}

	def getTables(self):
		tables = []
		for (k, v) in self._tableMap.items():
			v['tableName'] = k
			tables.append(v)
		return tables

	def updateTableGroups(self, nameField, tableGroups, tableName, index):
		'''
			更新TableGroups
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in tableGroups:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'groupKey' in table:
				logging.error('table `groupKey` conflict `%s` exists `%s`', field, table['group'])
				return False
			table['groupKey'] = field
			table['groupIndex'] = index
		return True

	def updateMapKeys(self, nameField, mapKeys, tableName, index):
		'''
			更新MapKeys
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in mapKeys:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'keyType' in table:
				logging.error('table `keyType` conflict `%s` exists `%s`', 'Map', table['keyType'])
				return False
			table['keyType'] = 'Map'
			table['key'] = field
			table['index'] = index
		return True

	def updateDecKeys(self, nameField, decKeys, tableName, index):
		'''
			更新DecKeys
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in decKeys:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'decKey' in table:
				logging.error('table `decKey` duplicate')
				return False
			table['decKey'] = True
		return True

	def updateListKeys(self, nameField, listKeys, tableName, index):
		'''
			更新ListKeys
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in listKeys:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'keyType' in table:
				logging.error('table `keyType` conflict `%s` exists `%s`', 'List', table['keyType'])
				return False
			table['keyType'] = 'List'
			table['key'] = field
			table['index'] = index
		return True

	def updateSortDowns(self, nameField, listKeys, tableName, index):
		'''
			更新updateSortDowns
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in listKeys:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			table['sort'] = 'down'
			table['sortKey'] = field
			table['sortIndex'] = index
		return True

	def updateSortUps(self, nameField, listKeys, tableName, index):
		'''
			更新updateSortDowns
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in listKeys:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			table['sort'] = 'up'
			table['sortKey'] = field
			table['sortIndex'] = index
		return True

	def updateObjectValues(self, nameField, objectValues, tableName, index):
		'''
			更新ObjectValues
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in objectValues:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'valueType' in table:
				logging.error('table `valueType` conflict `%s` exists `%s`', 'Object', table['valueType'])
				return False
			table['valueType'] = 'Object'
			table['value'] = field
		return True
	
	def updateArrayValues(self, nameField, arrayValues, tableName, index):
		'''
			更新ArrayValues
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in arrayValues:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'valueType' in table:
				logging.error('table `valueType` conflict `%s` exists `%s`', 'Array', table['valueType'])
				return False
			table['valueType'] = 'Array'
			table['value'] = field
		return True
	
	def updateSingleValues(self, nameField, singleValues, tableName, index):
		'''
			更新SingleValues
		'''
		field = nameField.getNote('field', nameField.getName())
		for t in singleValues:
			if t == '':
				t = tableName
			if t in self._tableMap:
				table = self._tableMap[t]
			else:
				table = {}
				self._tableMap[t] = table
			if 'valueType' in table:
				logging.error('table `valueType` conflict `%s` exists `%s`', 'Single', table['valueType'])
				return False
			table['valueType'] = 'Single'
			table['value'] = field
		return True

def compile(content, inPath, outPath):
	'''
		编译Table
	'''
	sheetDefine = NameField(content['sheetHead'][0])
	sheetName = content['sheet']
	tableName = sheetDefine.getNote('tableName', sheetName)
	fieldName = sheetDefine.getNote('field', sheetDefine.getName())

	# 收集 Table
	tableCollector = TableCollector()
	for i in range(0, len(content['sheetHead'])) :
		headData = content['sheetHead'][i]
		headField = NameField(headData)
		tableGroups = headField.getNotes('tableGroup')
		mapKeys = headField.getNotes('mapKey')
		decKeys = headField.getNotes('decKey')
		listKeys = headField.getNotes('listKey')
		sortDowns = headField.getNotes('sortDown')
		sortUps = headField.getNotes('sortUp')
		objectValues = headField.getNotes('objectValue')
		arrayValues = headField.getNotes('arrayValue')
		singleValues = headField.getNotes('singleValue')
		if len(tableGroups) > 0:
			if not tableCollector.updateTableGroups(headField, tableGroups, tableName, i):
				return False
		if len(mapKeys) > 0:
			if not tableCollector.updateMapKeys(headField, mapKeys, tableName, i):
				return False
		if len(decKeys) > 0:
			if not tableCollector.updateDecKeys(headField, decKeys, tableName, i):
				return False
		if len(listKeys) > 0:
			if not tableCollector.updateListKeys(headField, listKeys, tableName, i):
				return False
		if len(sortDowns) > 0:
			if not tableCollector.updateSortDowns(headField, sortDowns, tableName, i):
				return False
		if len(sortUps) > 0:
			if not tableCollector.updateSortUps(headField, sortUps, tableName, i):
				return False
		if len(objectValues) > 0:
			if not tableCollector.updateObjectValues(headField, objectValues, tableName, i):
				return False
		if len(arrayValues) > 0:
			if not tableCollector.updateArrayValues(headField, arrayValues, tableName, i):
				return False
		if len(singleValues) > 0:
			if not tableCollector.updateSingleValues(headField, singleValues, tableName, i):
				return False
	tableList = tableCollector.getTables()
	if len(tableList) <= 0:
		tableList.append({
			'tableName' : tableName,
			'keyType' : 'Map',
			'key' : fieldName,
			'valueType' : 'Object',
			'value' : fieldName,
		})

	# 生成 Table
	tablePath = os.path.join(outPath, 'compile', 'tables')
	if not os.path.exists(tablePath):
		os.makedirs(tablePath)
	for tc in tableList:
		_tableName = tc['tableName']
		_keyType = tc.get('keyType','Map')
		_decKey = tc.get('decKey', False)
		_key = tc.get('key',fieldName)
		_index = tc.get('index', 0)
		_sort = tc.get('sort', None)
		_sortKey = tc.get('sortKey', None)
		_sortIndex = tc.get('sortIndex', None)
		_valueType = tc.get('valueType','Object')
		_value = tc.get('value',fieldName)
		_groupKey = tc.get('groupKey')
		_groupIndex = tc.get('groupIndex')
		tableData = {
			'sheetHead' : content['sheetHead'],
			'tableConfig' : tc,
		}
		if _valueType == 'Object':
			groups = content['groupData'][_groupIndex] if _groupIndex != None else [ None ]
			for v in groups:
				nowTableName = _tableName if v == None else _tableName + toRealValue(v, TYPEFIELDS['string'])
				tableFile = os.path.join(tablePath, '%s.json' % nowTableName)
				if os.path.exists(tableFile):
					logging.error('table file conflict `%s` name `%s`' % (tableFile, nowTableName))
					return False
				
				_objectData = [ d for d in content['objectData'] if v == None or _groupKey == None or py.getDictPath(d,_groupKey) == v]
				if _keyType == 'Map':
					data = {}
					for d in _objectData:
						if _key in d:
							key = toRealValue(d[_key], TYPEFIELDS['string'])
							if py.isNumber(str(key)):
								data[key] = d
							else:
								py.setDictPath(data, key, d)
					tableData['cache'] = { nowTableName : data }			
					if _decKey:
						tableData['cache']["#declareMapKey"] = {
							nowTableName : True
						}
				else: # List
					data = list(_objectData)
					#if _sort != None:
					#	data.sort(key=lambda x:x[_sortKey], reverse=_sort.upper()!='UP')
					tableData['cache'] = { nowTableName : data }
					if _sort != None:
						tableData['cache']["#listSort"] = {	
							nowTableName : {
								"key" : _sortKey,
								"type" : _sort
							}
						}
				py.writeJson(tableData, tableFile, True)
		elif _valueType == 'Array':
			groups = content['groupData'][_groupIndex] if _groupIndex != None else [ None ]
			for v in groups:
				nowTableName = _tableName if v == None else _tableName + toRealValue(v, TYPEFIELDS['string'])
				tableFile = os.path.join(tablePath, '%s.json' % nowTableName)
				if os.path.exists(tableFile):
					logging.error('table file conflict `%s` name `%s`' % (tableFile, nowTableName))
					return False
				
				_sheetData = [ d for d in content['sheetData'] if v == None or _groupIndex == None or _groupIndex < len(d) or d[_groupIndex] == v]
				if _keyType == 'Map':
					data = {}
					for d in _sheetData:
						if len(d) > _index:
							key = toRealValue(d[_index], TYPEFIELDS['string'])
							if py.isNumber(str(key)):
								data[key] = d
							else:
								py.setDictPath(data, key, d)
					tableData['cache'] = { nowTableName : data }
					if _decKey:
						tableData['cache']["#declareMapKey"] = {
							nowTableName : True
						}
				else: # List
					data = list(_sheetData)
					#if _sort != None:
					#	data.sort(key=lambda x:x[_sortIndex], reverse=_sort.upper()!='UP')
					tableData['cache'] = { nowTableName : data }
					if _sort != None:
						tableData['cache']["#listSort"] = {	
							nowTableName : {
								"key" : _sortIndex,
								"type" : _sort
							}
						}
				py.writeJson(tableData, tableFile, True)
		else: # Single
			groups = content['groupData'][_groupIndex] if _groupIndex != None else [ None ]
			for v in groups:
				nowTableName = _tableName if v == None else _tableName + toRealValue(v, TYPEFIELDS['string'])
				tableFile = os.path.join(tablePath, '%s.json' % nowTableName)
				if os.path.exists(tableFile):
					logging.error('table file conflict `%s` name `%s`' % (tableFile, nowTableName))
					return False
				
				_objectData = [ d for d in content['objectData'] if v == None or _groupKey == None or py.getDictPath(d,_groupKey) == v]
				if _keyType == 'Map':
					data = {}
					for odata in _objectData:
						key = py.getDictPath(odata,_key)
						value = py.getDictPath(odata,_value)
						if key != None and value != None:
							key = toRealValue(key, TYPEFIELDS['string'])
							if py.isNumber(str(key)):
								data[key] = value
							else:
								py.setDictPath(data, key, value)
					tableData['cache'] = { nowTableName : data }
					if _decKey:
						tableData['cache']["#declareMapKey"] = {
							nowTableName : True
						}
				else: # List
					tempdata = list(_objectData)
					data = [ py.getDictPath(d, _value) for d in tempdata ]
					#if _sort != None:
					#	data.sort(key=lambda x:x, reverse=_sort.upper()!='UP')
					tableData['cache'] = { nowTableName : data }
					if _sort != None:
						tableData['cache']["#listSort"] = {	
							nowTableName : {
								"type" : _sort
							}
						}
				py.writeJson(tableData, tableFile, True)

	return True

