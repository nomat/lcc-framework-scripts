# -*- coding: utf-8 -*-

import os,re,logging
import ply.lex as lex
import ply.yacc as yacc
from ...utils import py

SOURCE_FILE = None

#--------------------- lexer ----------------------

# 关键字
keyWords = {
	"export" 	: "K_EXPORT",
	"struct" 	: "K_STRUCT",
	"interface" : "K_INTERFACE",
	"message" 	: "K_MESSAGE",
	"extends" 	: "K_EXTENDS",
	"enum" 		: "K_ENUM",
	"number"	: "K_NUMBER",
	"string"	: "K_STRING",
	"boolean"	: "K_BOOLEAN",
	"Uint8Array": "K_UINT8ARRAY",
	"key"		: "K_KEY",
	"float"		: "K_FLOAT",
	"double"	: "K_DOUBLE",
	"int32"		: "K_INT32",
	"int64"		: "K_INT64",
	"uint32"	: "K_UINT32",
	"uint64"	: "K_UINT64",
	"sint32"	: "K_SINT32",
	"sint64"	: "K_SINT64",
	"fixed32"	: "K_FIXED32",
	"fixed64"	: "K_FIXED64",
	"sfixed32"	: "K_SFIXED32",
	"sfixed64"	: "K_SFIXED64",
	"bool"		: "K_BOOL",
	"bytes"		: "K_BYTES",
	"map"		: "K_MAP",
}

# token
tokens = [
	"ID",
	"NUMBER",
	"STRING",
] + list(keyWords.values())

# 常量token
literals = '#{}[]():=+-*/%?;,<>'

# 转义字符表
escapes = {
	"a" : "\a",
	"b" : "\b",
	"f" : "\f",
	"n" : "\n",
	"r" : "\r",
	"t" : "\t",
	"v" : "\v",
	"\\" : "\\",
	"'" : "'",
	"\"" : "\"",
	"?" : "\?",
}

# 转义正则表达式
RE_ESCAPE_OCT = re.compile(r'\\[0-7]{1,3}')
RE_ESCAPE_HEX = re.compile(r'\\x[0-9A-Fa-f]{1,2}')
RE_ESCAPE_CHR = re.compile(r'\\[abfnrtv\'\"?]')

#------------- rules

t_ignore_space  		= r'[ \t]'

# 注释处理
def t_comment_1(t):
	r'/\*{1,2}[\s\S]*?\*/'
	t.lexer.lineno += t.value.count('\n')
def t_comment_2(t):
	r'//[\s\S]*?\n'
	t.lexer.lineno += t.value.count('\n')
def t_comment_3(t):
	r'//[\s\S]*?'
	t.lexer.lineno += t.value.count('\n')

# 分隔符
def t_newline(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

# 标识符
def t_ID(t):
	r'[a-zA-Z_][a-zA-Z_0-9]*'
	t.type = keyWords.get(t.value,'ID')
	return t
	
# NUMBER
def t_NUMBER(t):
	r'([0-9]*[.]?[0-9]+|[0-9]+[.])([E|e][-+]?[0-9]+)?|[0-9]+'
	t.value = float(t.value)	
	return t

# STRING
def t_STRING(t):
	r'"(\\"|[^\n"])+"'
	t.value = py.loadCString(t.value[1:-1])
	return t

# 错误处理
def t_error(t):
	raise Exception('Error {} at line {} '.format(t.value[0], str(t)))
	
# 构建词法分析器
lexer = lex.lex()

#-------------------- parser ----------------------
	
# 优先级、结合性
precedence = (
	('left', '+', '-'),
	('left', '*', '/', '%'),
	('right', 'UMINUS'),	# 负号
	('right', '('),
	('left', ')'),
)

#------------- rules

start = 'root'

# 错误处理
def p_error(p):
	if p:
		raise SyntaxError("%s(%d) : %s " % (SOURCE_FILE, p.lexer.lineno, str(p.value)))
	else:
		raise SyntaxError("syntax error at EOF")
	
# 根
def p_root_1(p):
	''' root : struct ','
			 | struct ';'
			 | struct
	'''
	p[0] = []
	if p[1]:
		p[0].append(p[1])
def p_root_2(p):
	''' root : root struct ','
			 | root struct ';'
			 | root struct
	'''
	if p[2]:
		p[1].append(p[2])
	p[0] = p[1]
	
# 表达式，必须常量
def p_expression_1(p):
	''' expression : NUMBER
	'''
	p[0] = p[1]
def p_expression_2(p):
	''' expression : '-' expression %prec UMINUS
	'''
	p[0] = -p[2]
def p_expression_3(p):
	''' expression : '(' expression ')'
	'''
	p[0] = p[2]
def p_expression_4(p):
	''' expression : expression '+' expression
	'''
	p[0] = p[1] + p[3]
def p_expression_5(p):
	''' expression : expression '-' expression
	'''
	p[0] = p[1] - p[3]
def p_expression_6(p):
	''' expression : expression '*' expression
	'''
	p[0] = p[1] * p[3]
def p_expression_7(p):
	''' expression : expression '/' expression
	'''
	p[0] = p[1] / p[3]
def p_expression_8(p):
	''' expression : expression '%' expression
	'''
	p[0] = p[1] % p[3]

# 结构
def p_struct_1(p):
	''' struct : message
			   | enum
	'''
	p[0] = p[1]

# 消息
def p_message_1(p):
	''' message : K_STRUCT ID '{' messageFields '}'
				| K_INTERFACE ID '{' messageFields '}'
				| K_MESSAGE ID '{' messageFields '}'
	'''
	global SOURCE_FILE
	p[0] = {
		'source' : SOURCE_FILE,
		"type" : "message",
		'command' : 0,
		'export' : False,
		'name' : p[2],
		'fields' : p[4],
	}
	logging.debug('-- message %s' % str(p[0]))
def p_message_2(p):
	''' message : K_EXPORT '(' NUMBER ')' K_STRUCT ID '{' messageFields '}'
				| K_EXPORT '(' NUMBER ')' K_INTERFACE ID '{' messageFields '}'
				| K_EXPORT '(' NUMBER ')' K_MESSAGE ID '{' messageFields '}'
	'''
	global SOURCE_FILE
	if not p[3].is_integer():
		raise Exception("protocol command must be ", int)
	p[0] = {
		'source' : SOURCE_FILE,
		"type" : "message",
		'command' : int(p[3]),
		'export' : True,
		'name' : p[6],
		'fields' : p[8],
	}
	logging.debug('-- message %s' % str(p[0]))
def p_message_3(p):
	''' message : K_STRUCT ID K_EXTENDS ID '{' messageFields '}'
				| K_INTERFACE ID K_EXTENDS ID '{' messageFields '}'
				| K_MESSAGE ID K_EXTENDS ID '{' messageFields '}'
	'''
	global SOURCE_FILE
	p[0] = {
		'source' : SOURCE_FILE,
		"type" : "message",
		'command' : 0,
		'export' : False,
		'name' : p[2],
		'parent' : p[4],
		'fields' : p[6],
	}
	logging.debug('-- message %s' % str(p[0]))
def p_message_4(p):
	''' message : K_EXPORT '(' NUMBER ')' K_STRUCT ID K_EXTENDS ID '{' messageFields '}'
				| K_EXPORT '(' NUMBER ')' K_INTERFACE ID K_EXTENDS ID '{' messageFields '}'
				| K_EXPORT '(' NUMBER ')' K_MESSAGE ID K_EXTENDS ID '{' messageFields '}'
	'''
	global SOURCE_FILE
	if not p[3].is_integer():
		raise Exception("protocol command must be ", int)
	p[0] = {
		'source' : SOURCE_FILE,
		"type" : "message",
		'command' : int(p[3]),
		'export' : True,
		'name' : p[6],
		'parent' : p[8],
		'fields' : p[10],
	}
	logging.debug('-- message %s' % str(p[0]))

# 消息域组
def p_messageFields_1(p):
	''' messageFields : messageField ','
					  | messageField ';'
					  | messageField
	'''
	p[0] = []
	if p[1]:
		p[0].append(p[1])
def p_messageFields_3(p):
	''' messageFields : messageFields messageField ','
					  | messageFields messageField ';'
					  | messageFields messageField
	'''
	if p[2]:
		p[1].append(p[2])
	p[0] = p[1]

# 域
def p_messageField_1(p):
	''' messageField : fieldName ':' fieldOption
	'''
	p[0] = p[1]
	p[0].update(p[3])

# 域名
def p_fieldName_1(p):
	''' fieldName : ID '?'
	'''
	p[0] = {
		'name' : p[1],
		'option' : True,
	}
def p_fieldName_2(p):
	''' fieldName : ID
	'''
	p[0] = {
		'name' : p[1],
		'option' : False,
	}

# 域选项
def p_fieldOption_1(p):
	''' fieldOption : fieldTypeA '=' NUMBER
	'''
	if not p[3].is_integer():
		raise Exception("@ must be ", int)
	p[0] = {
		'type' : p[1],
		'location' : int(p[3]),
	}
def p_fieldOption_2(p):
	''' fieldOption : fieldTypeA
	'''
	p[0] = {
		'type' : p[1]
	}

# 域全类型
def p_fieldTypeA_1(p):
	''' fieldTypeA : fieldType 
	'''
	p[0] = p[1]
def p_fieldTypeA_2(p):
	''' fieldTypeA : fieldType '[' ']'
	'''
	p[0] = {
		'_group' : True,
		'type' : p[1],
	}
def p_fieldTypeA_3(p):
	''' fieldTypeA : K_MAP '<' fieldType ',' fieldType '>'
	'''
	p[0] = {
		'_map' : True,
		'key' : p[3],
		'value' : p[5],
	}
def p_fieldTypeA_4(p):
	''' fieldTypeA : '{' '[' K_KEY ':' fieldType ']' ':' fieldType '}'
	'''
	p[0] = {
		'_map' : True,
		'key' : p[5],
		'value' : p[8],
	}

# 域类型
def p_fieldType_ID(p):
	''' fieldType : ID 
	'''
	p[0] = {
		'ts' : p[1],
		'p3' : p[1],
	}
def p_fieldType_string(p):
	''' fieldType : K_STRING
	'''
	p[0] = {
		'ts' : p[1],
		'p3' : p[1],
	}
def p_fieldType_boolean(p):
	''' fieldType : K_BOOLEAN
	'''
	p[0] = {
		'ts' : p[1],
		'p3' : 'bool',
	}
def p_fieldType_bool(p):
	''' fieldType : K_BOOL
	'''
	p[0] = {
		'ts' : 'boolean',
		'p3' : p[1],
	}
def p_fieldType_Uint8Array(p):
	''' fieldType : K_UINT8ARRAY
	'''
	p[0] = {
		'ts' : 'Uint8Array|string',
		'p3' : 'bytes',
	}
def p_fieldType_bytes(p):
	''' fieldType : K_BYTES
	'''
	p[0] = {
		'ts' : 'Uint8Array|string',
		'p3' : p[1],
	}
def p_fieldType_number(p):
	''' fieldType : K_NUMBER 
	'''
	p[0] = {
		'ts' : p[1],
		'p3' : 'int32',
	}
def p_fieldType_float(p):
	''' fieldType : K_FLOAT 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_double(p):
	''' fieldType : K_DOUBLE 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_int32(p):
	''' fieldType : K_INT32 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_int64(p):
	''' fieldType : K_INT64 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_uint32(p):
	''' fieldType : K_UINT32 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_uint64(p):
	''' fieldType : K_UINT64 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_sint32(p):
	''' fieldType : K_SINT32 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_sint64(p):
	''' fieldType : K_SINT64 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_fixed32(p):
	''' fieldType : K_FIXED32 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_fixed64(p):
	''' fieldType : K_FIXED64 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_sfixed32(p):
	''' fieldType : K_SFIXED32 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}
def p_fieldType_sfixed64(p):
	''' fieldType : K_SFIXED64 
	'''
	p[0] = {
		'ts' : 'number',
		'p3' : p[1],
	}

# 枚举
def p_enum_1(p):
	''' enum : K_ENUM ID '{' enumFields '}'
	'''
	global SOURCE_FILE
	p[0] = {
		'source' : SOURCE_FILE,
		"type" : "enum",
		'name' : p[2],
		'fields' : p[4],
	}

# 枚举域组
def p_enumFields_1(p):
	''' enumFields : enumField ','
				   | enumField ';'
				   | enumField
	'''
	p[0] = []
	if p[1]:
		p[0].append(p[1])
def p_enumFields_2(p):
	''' enumFields : enumFields enumField ','
				   | enumFields enumField ';'
				   | enumFields enumField
	'''
	if p[2]:
		p[1].append(p[2])
	p[0] = p[1]

# 枚举域
def p_enumField_1(p):
	''' enumField : ID
	'''
	p[0] = {
		'name' : p[1]
	}
def p_enumField_2(p):
	''' enumField : ID '=' expression
	'''
	if not p[3].is_integer():
		raise Exception("enum must be ", int)
	p[0] = {
		'name' : p[1],
		'value' : int(p[3])
	}
	
# 构建语法分析器
parser = yacc.yacc()

#--------------------------

def parseProtoFile(protoFile):
	'''
		解析协议文件
	'''
	global SOURCE_FILE
	SOURCE_FILE = protoFile
	with open(protoFile, 'r', encoding='utf8') as f:
		return parser.parse(f.read(), lexer = lexer)
