# -*- coding: utf-8 -*-

import sys,os,json,logging

# 脚本路径
SCRIPT_PATH = os.path.split(os.path.realpath(__file__))[0]

# 基本路径
BASE_PATH = os.path.join(SCRIPT_PATH, '..')

# 构建目录
BUILD_PATH = os.path.join(BASE_PATH, 'build')

# 构建缓存目录
BUILD_CACHES_PATH = os.path.join(BUILD_PATH, 'caches')

# 构建发布目录
BUILD_PUBLISH_PATH = os.path.join(BUILD_PATH, 'publishs')

# 临时目录
TEMP_PATH = os.path.join(BASE_PATH, 'temp')

# 配置文件
CONFIG_FILE = os.path.join(BASE_PATH, 'config.json')

if not os.path.isdir(TEMP_PATH):
	os.makedirs(TEMP_PATH)

#日志配置
logging.basicConfig(
	level = logging.INFO,
	format = '%(asctime)s %(levelname)s %(message)s',
	datefmt = '%Y-%m-%d %H:%M:%S',
	#filename = os.path.join(BASE_PATH, 'log.txt'),
	#filemode = 'w',
)
