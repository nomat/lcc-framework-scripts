# -*- coding: utf-8 -*-

import os,shutil,logging
from .. import config as C
from .. import utils
from ..utils import py
from .lang import publish as publishLang
from .table import publish as publishTable
from .asset import publish as publishAsset
from .protocol import publish as publishProto

'''
	发布资源
'''
def publish():
	
	publishLang() and \
	publishTable() and \
	publishAsset() and \
	publishProto()
	